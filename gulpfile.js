"use strict";

var gulp = require("gulp");
var less = require("gulp-less");
var plumber = require("gulp-plumber");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var svgstore = require("gulp-svgstore");

var minify = require("gulp-csso");
var rename = require("gulp-rename");
var imagemin = require("gulp-imagemin");
var webp = require("gulp-webp");
var posthtml = require("gulp-posthtml");
var include = require("posthtml-include");
var del = require("del");
var run = require("run-sequence");

var server = require("browser-sync").create();

/* Обработка less файлов, сборка одного css */
gulp.task("style", function() {
  gulp.src("build/css/index.less")
    .pipe(plumber())
    .pipe(less())
    .pipe(postcss([
      autoprefixer()
    ]))
    .pipe(gulp.dest("static/css"))

    /* Минимизация css */
    .pipe(minify())
    .pipe(rename("style.min.css"))
    .pipe(gulp.dest("static/css"))
    /*.pipe(gulp.dest("css"))*/

    .pipe(server.stream());
});

/* Создание спрайта */
gulp.task("sprite", function(){
  return gulp.src("build/svg/*.svg")
  .pipe(svgstore({
    inlineSvg: true
  }))
  .pipe(rename("sprite.svg"))
  .pipe(gulp.dest("static/img"));
});

/* Вставка спрайта во все html файлы */
gulp.task("html", function(){
  return gulp.src("templates/*.html")
    .pipe(posthtml([
      include()
    ]))

    .pipe(gulp.dest("templates"));
});

gulp.task("style_admin", function() {
  gulp.src("build/css/admin.less")
    .pipe(plumber())
    .pipe(less())
    .pipe(postcss([
      autoprefixer()
    ]))
    .pipe(gulp.dest("static/css"))

    /* Минимизация css */
    .pipe(minify())
    .pipe(rename("style_admin.min.css"))
    /*.pipe(gulp.dest("static/css"))
    .pipe(gulp.dest("css"))*/

    .pipe(server.stream());
});

/* Копирование других файлов */
gulp.task("copy", function(){
  return gulp.src([
    "fonts/**/*.{woff,woff2}",
    "img/**",
    "js/**"
  ], {
    base: "."
  })
  .pipe(gulp.dest("build"));
});

/* Удаление старых данных из папки разработки */
gulp.task("clean", function(){
  return del("static");
});

gulp.task('directory', function () {
    return gulp.src('*', {read: false})
    .pipe(shell([
      'mkdir -p  static'
    ]));
});

gulp.task("build", function (done) {
  run("copy", "style", "style_admin", "sprite", "html",  done);
});

gulp.task("serve", function() {
  server.init({
    server: "build/",
    notify: false,
    open: true,
    cors: true,
    ui: false
  });

  gulp.watch("less/**/*.less", ["style"]);
  gulp.watch("*.html", ["html"]);
});