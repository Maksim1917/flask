from flask import render_template
from flask import request
from flask import Flask
from flask import redirect
from flask import jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, current_user, login_user, logout_user, login_required
from werkzeug.utils import secure_filename
import os
import config.config as config


#from werkzeug.security import generate_password_hash
#password_hash = generate_password_hash('123')
#print(password_hash)

basedir = os.path.abspath(os.path.dirname(__file__))


UPLOAD_FOLDER = 'static/img/'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])


app = Flask('Web')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+ os.path.join(basedir, 'test.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = '9f{K?HlagyHI~MN~0PdF~bKWzmu7DvQG'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['FLASK_DEBUG'] = 1
db = SQLAlchemy(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
#from models import User
#admin = User(username='admin', email='admin@example.com', password_hash = password_hash)
#user = User(username='user', email='user@example.com')
#db.create_all()

#db.session.add(admin)
#db.session.add(user)
#db.session.commit()
#User.query.all()

#user = User.query.filter_by(username='admin').first()

#user.password_hash = password_hash
#print(user.password_hash)
#db.session.add(user)
#db.session.commit()

"""
    @   Проверка загружаемого файла по расширению
    @
"""
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


"""
    @   Главная страница
    @
"""
@app.route('/', methods=['GET', 'POST'])
def index():
    from models import User, Category
    error = None
    categories = Category.query.all()
    if request.method == 'POST':
        """
        Обработка 
        """
    else:
        print(request.args.get("time"))
    return render_template('index.html', error = error,
                                         meta_description = config.meta_description,
                                         meta_title = config.meta_title,
                                         categories = categories,
                                         time = request.args.get("time"),)
"""
    @   Аунтификация пользователя
    @
"""
@app.route('/login', methods=['GET', 'POST'])
def login():
    from models import User

    if current_user.is_authenticated:
        return redirect('/dashboard/')
    else:
        if request.method == 'POST':
            username = request.form['username']
            password = request.form['password']
            user = User.query.filter_by(username=username).first()
            if User.check_password(user, password):
                try:
                    login_user(user, remember=request.form['remember'])
                except:
                    login_user(user)
                return redirect('/dashboard/')
            else:
                return render_template('login.html', message="Пароль неверен")
        else:
            return render_template('login.html', message="Введите логин и пароль")

"""
    @   Разлогирование пользователя
    @
"""
@app.route('/logout/', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect('/')

"""
    @   Админ панель
    @
"""
@app.route('/dashboard/', methods=['GET', 'POST'])
@login_required
def dashboard():
    from models import Post, Category
    title = 'Админ панель'
    posts = Post.query.all()
    categories = Category.query.all()


    return render_template('dashboard.html', title = title,
                                             posts = posts,
                                             categories = categories,)

"""
    @   Добавление категории catAdd
    @
"""
@app.route('/dashboard/catAdd/', methods=['GET', 'POST'])
@login_required
def categories():
    from models import User, Category
    title = 'Добавление категории'

    if request.method == 'POST':
        print(request.form['category'])
        if request.form['category'] is not None:
            category = Category(title=request.form['category'])
            db.session.add(category)
            db.session.commit()
            return redirect('/dashboard/')
        else:
            return redirect('/dashboard/catAdd/')
    else:
        return render_template('categoryAdd.html', title = title)
"""
    @   Добавление новости postAdd
    @
"""
@app.route('/dashboard/postAdd/', methods=['GET', 'POST'])
@login_required
def posts():
    from models import User, Category, Post
    title = 'Добавление поста'

    if request.method == 'POST':
        if request.form['title'] is not None:
            catId = Category.query.filter(Category.title == request.form['category_id']).first()
            print(catId.id)
            if 'file' not in request.files or allowed_file('file'):
                file = ''
            file = request.files['file']
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            post = Post(cat_id = catId.id,
                            title = request.form['title'],
                            content = request.form['content'],
                            file = filename,
                            )
            db.session.add(post)
            db.session.commit()
            return redirect('/dashboard/')
        else:
            return redirect('/dashboard/postAdd/', )
    else:
        categories = Category.query.all()
        return render_template('postAdd.html', categories = categories,
                                               title = title,)

"""
    @   Обработка команд из терминала
    @
"""
@app.route('/_terminal_send', methods=['GET', 'POST'])
def add_numbers():
    from models import Post, Category

    a = request.args.get('a', 0, type=str)
    b = request.args.get('b', 0, type=str)

    Cat = Category.query.filter(Category.title == a).first()
    if Cat is not None:
        #Posts = Post.query.filter(Post.cat_id == Cat.id).all()
        Posts = Cat.posts.all()
        print(Posts)
        print(Posts[0].content)
        print(Posts[1].content)


        rest_json = []
        for post in Posts:
            rest_json.append(post.jsond())
        print(rest_json)
        return jsonify( result = rest_json)
    else:
        return jsonify()

"""
    @   Страница ошибки
    @
"""
@app.errorhandler(404)
def not_found(error):
    return render_template('error.html', error = error), 404


if __name__ == '__main__':
    FLASK_DEBUG = 1
    app.run(debug = True)
