from datetime import date
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin

from app import db, login_manager

"""
    @ Модель пользователя  
    @
"""
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password_hash = db.Column(db.String(128))

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __str__(self):
        return '<User %r>' % self.username

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))

"""
    @ Модель категории  
    @
"""
class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(80), unique=True, nullable=False)
    posts = db.relationship('Post', backref='CatParent', lazy='dynamic')

    def __str__(self):
        return '<Category %r>' % self.title

"""
    @ Модель записи  
    @
"""
class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    cat_id = db.Column(db.Integer, db.ForeignKey('category.id'))

    title = db.Column(db.String(140), unique=True, nullable=False)
    content = db.Column(db.String(3000), nullable=False)
    file = db.Column(db.String(140), nullable=False)
    date_created = db.Column(db.Date, default=date.today)
    is_visible = db.Column(db.Boolean, default=True)

    def __str__(self):
        return '<Post %r>' % self.title

    def jsond(self):
        instDict = self.__dict__.copy()
        if  '_sa_instance_state' in instDict:
            del instDict['_sa_instance_state']
        return instDict