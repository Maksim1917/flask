$(function() {
    /* При загрузке страницы переместить фокус на инпут терминала. */

    const resultTemplate = document.querySelector('#tmpl').content;
    const postsTemplate = document.querySelector('#postmpl').content;
    const KEYCODE = { ENTER: 13 };

    $('#terminal-input').bind('keypress', function(event ) {
        if ( event.which == KEYCODE.ENTER) {
            $.getJSON($SCRIPT_ROOT + '/_terminal_send', {
                a: $('input[name="a"]').val()
            }, function(data) {
                const result = window.serverResponseMock.result;

                result.forEach(post => {
                   const postDOMElement = postsTemplate.cloneNode(true);
                   const { title, is_visible } = post;
                   $(postDOMElement).find('span').append(title);

                   if (is_visible) {
                     $('.terminal-window_empty').append(postDOMElement);
                   }
                });
                $("#result").text(data.result);
            });
            /* Очистить значение value в input.
             * Создать span со значением поля value,
             * чтобы получить эффект проматывани терминала при введении команд в терминал.
             * Все что сверху должно пропадать из виду. Либо скрываться, либо удаляться.
             */
            return false;
        }
    });
});