var serverResponseMock = {
  "result": [
    {
      "cat_id": 4,
      "content": "Content example 1",
      "date_created": "Fri, 20 Jul 2018 00:00:00 GMT",
      "id": 3,
        "file": "517481803.jpg",
      "is_visible": true,
      "title": "123123123",

    },
    {
      "cat_id": 4,
      "content": "Content example 2",
      "date_created": "Fri, 23 Jul 2018 00:00:00 GMT",
      "id": 4,
        "file": "cherry-blossom-backgrounds-1920x1200_1.jpg",
      "is_visible": true,
      "title": "123456789"
    },
    {
      "cat_id": 4,
      "content": "Content example 3",
      "date_created": "Fri, 23 Jul 2018 00:00:00 GMT",
      "id": 4,
        "file": "cherry-blossom-backgrounds-1920x1200_1.jpg",
      "is_visible": true,
      "title": "asdasdas"
    }
  ]
};